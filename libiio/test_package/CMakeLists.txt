cmake_minimum_required(VERSION 3.15)

project(libiio-test CXX)

include(${CMAKE_CURRENT_BINARY_DIR}/conan_paths.cmake)

add_executable(libiio-test main.cpp)

find_package(libiio REQUIRED)
target_link_libraries(libiio-test
    PRIVATE libiio::libiio
)
