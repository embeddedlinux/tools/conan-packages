from conans import ConanFile, AutoToolsBuildEnvironment, tools
import os

class LibgpiodConan(ConanFile):
    name = "libgpiod"
    license = "LGPL-2.1-or-later"
    author = "Kuba Sejdak kuba.sejdak@gmail.com"
    url = "https://gitlab.com/embeddedlinux/tools/conan-packages"
    homepage = "https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/"
    description = "C/C++ library and tools for interacting with the linux GPIO character device"
    topics = ("gpio", "linux", "driver")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "cmake_find_package"
    options = {
        "shared": [True, False],
        "build_cxx": [True, False]
    }
    default_options = {
        "shared": False,
        "build_cxx": True
    }

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("libgpiod-" + self.version, "libgpiod")

    def build_requirements(self):
        installer = tools.SystemPackageTool()
        installer.install("autoconf-archive")

    def build(self):
        self.run("autoreconf --force --install --verbose", cwd="libgpiod")

        cfgArgs = []
        if self.options.shared:
            cfgArgs += ["--enable-shared", "--disable-static"]
        else:
            cfgArgs += ["--disable-shared", "--enable-static"]

        if self.options.build_cxx:
            cfgArgs += ["--enable-bindings-cxx"]

        autotools = AutoToolsBuildEnvironment(self)
        envVars = autotools.vars
        envVars["ac_cv_func_malloc_0_nonnull"] = "yes"
        autotools.configure(configure_dir="libgpiod", args=cfgArgs, vars=envVars)
        autotools.make()
        autotools.install()

    def package(self):
        self.copy(pattern="COPYING", src="libgpiod", keep_path=False)
        self.copy("*.h", dst="include", src="libgpiod/include")
        if self.options.build_cxx:
            self.copy("*.hpp", dst="include", src="libgpiod/bindings/cxx")

        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["gpiod"]
        if self.options.build_cxx:
            self.cpp_info.libs += ["gpiodcxx"]

    def package_id(self):
        del self.info.settings.build_type
